<?php

namespace App\Library;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class WikiInitialParagraph
{

    private $data;

    private $client;



    public function __construct()
    {
        $this->client = new Client(['base_uri' => "https://en.wikipedia.org/w/api.php"]);
    }



    public function InitialParagraph($country)
    {
        try {

            $response = $this->client->get( '?action=query&titles=' . "$country" . '&prop=extracts&format=json&exintro=1')
                ->getBody()
                ->getContents();

            $response = json_decode($response);

            return $this->get_list_of_item($response->query->pages);

        } catch (\Exception $exception) {

            return response()->json(['Exception' => $exception->getMessage()], 400);
        }
    }

    private function get_list_of_item($wiki)
    {
        try {
            $result = new Collection();

            foreach ($wiki as $page_id => $item) {

                $result[$page_id] = $item->extract;
            }

            foreach ($result as $res) {
                $this->data = strip_tags(preg_replace('/<[^>]*>/', '',
                    str_replace(array("&nbsp;", "\n", "\r"), "", html_entity_decode($res, ENT_QUOTES, 'UTF-8'))));

            }

            return $this->data;

        } catch (\Exception $exception) {

            return response()->json(['Exception' => $exception->getMessage()], 400);
        }
    }

}
<?php

namespace App\Library;


use App\Service\Countries;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use App\Library\WikiInitialParagraph as Wiki;

class MostPopularYouTube
{

    private $wiki;

    private $countries;

    private $client;

    private $key;


    public function __construct(Wiki $wiki, Countries $countries)
    {
        $this->wiki = $wiki;

        $this->countries = $countries;

        $this->client = new Client(['base_uri' => "https://www.googleapis.com/youtube/v3/videos"]);

        $this->key = env('YOUTUBE_API_KEY');

    }

    private function mostPopular($country)
    {

        try {

            $promise = ['data' => $this->client->getAsync('?part=snippet&chart=mostPopular&maxResults=1&regionCode=' . "$country" . '&key=' . $this->key)];

            $results = Promise\settle($promise)->wait();

            return $results['data']['value']->getBody()->getContents();

        }catch (\Exception $exception){

            return response()->json(['Exception' => $exception->getMessage()], 400);
        }
    }

    public function getPopular()
    {
        try {
            $result = [];

            $countries = $this->countries->countries;

            foreach ($countries as $code => $country) {

                $result[$country]['wiki'] = $this->wiki->InitialParagraph($country);

                $videoList = $this->mostPopular($code);

                $items = json_decode($videoList)->items;

                foreach ($items as $list => $video) {

                    $result[$country]['video_description'] = strip_tags(preg_replace('/<[^>]*>/', '',
                        str_replace(array("&nbsp;", "\n", "\r"), "",
                            html_entity_decode($video->snippet->description, ENT_QUOTES, 'UTF-8'))));

                    $result[$country]['thumb_normal'] = $video->snippet->thumbnails->default;

                    $result[$country]['thumb_high'] = $video->snippet->thumbnails->high;

                }
            }

            return $result;

        } catch (\Exception $exception) {

            return response()->json(['Exception' => $exception->getMessage()], 400);
        }
    }


}
<?php

namespace App\Service;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Paginate
{

    public function paginate($result, $perPage)
    {
        try {

            $data_collection = new Collection($result);

            $current_page = LengthAwarePaginator::resolveCurrentPage();

            $current_page_data = $data_collection->slice(($current_page - 1) * $perPage, $perPage)->all();

            $data_to_show = new LengthAwarePaginator($current_page_data, count($data_collection), $perPage);

            return $data_to_show;

        }catch (\Exception $exception){

            return response()->json(['Exception' => $exception->getMessage()], 400);

        }
    }

}
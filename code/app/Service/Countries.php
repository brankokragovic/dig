<?php

namespace App\Service;


class Countries
{

    public $countries = [
        'ua' => 'Ukraine',
        'nl' => 'Netherlands',
        'de' => 'Denmark',
        'fr' => 'France',
        'es' => 'Spain',
        'it' => 'Italy',
        'gr' => 'Greece'
    ];

}
<?php

namespace App\Http\Controllers;

use App\Service\FetchData;

class DataController extends Controller
{

    private $data;

    public function __construct(FetchData $fetchData)
    {
        $this->data = $fetchData;
    }

    public function getData($perPage)
    {
        try {

            $response = $this->data->fetch($perPage);

            return $response;

        }catch (\Exception $exception){

            return response()->json(['Exception' => $exception->getMessage()], 400);

        }

    }

}

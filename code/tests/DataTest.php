<?php

class DataTest extends TestCase
{

    /**
     * /getData/perPage [GET]
     */
    public function testShouldReturnJsonData()
    {
        $this->get("getData/1");
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'current_page' => [
                    'data' =>
                        [
                            'Ukraine' => [
                                'wiki',
                                'video_description',
                                'thumb_normal' => [
                                    "url",
                                    "width",
                                    "height"
                                ],
                                'thumb_high' => [
                                    "url",
                                    "width",
                                    "height"
                                ]
                            ]
                        ],
                    "first_page_url",
                    "from",
                    "last_page",
                    "last_page_url",
                    "next_page_url",
                    "path",
                    "per_page",
                    "prev_page_url",
                    "to",
                    "total"
                ]
            ]);

    }
}